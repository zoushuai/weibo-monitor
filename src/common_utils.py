import smtplib
from email.mime.text import MIMEText
from email.header import Header


# 发送邮件函数
def sendInfo(receiver, text):
    # 三个参数：第一个为文本内容，第二个 plain 设置文本格式，第三个 utf-8 设置编码
    message = MIMEText(text, 'html', 'utf-8')
    message['From'] = Header("白菜来了", 'utf-8')  # 发送者
    message['To'] = Header("菜鸡", 'utf-8')  # 接收者

    subject = '白菜来了'
    message['Subject'] = Header(subject, 'utf-8')

    try:

        # windows 下这样写
        # smtpObj = smtplib.SMTP()
        # smtpObj.connect('smtp.qq.com', 25)  # 25 为 SMTP 端口号
        # Linux 下这样写
        smtpObj = smtplib.SMTP_SSL()
        smtpObj.connect('smtp.qq.com', 465)     # 阿里云主机只允许465 为 SSL 端口号

        smtpObj.login('xxxxx@qq.com', 'xxxxxxx')    # 邮箱账号和授权码
        smtpObj.sendmail('xxxx@qq.com', receiver, message.as_string())
        print("邮件发送成功")
    except smtplib.SMTPException:
        print("Error: 无法发送邮件")

